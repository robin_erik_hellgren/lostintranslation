import { Redirect, Route } from "react-router-dom/cjs/react-router-dom.min"

function PrivateRoute({children, ...rest}) {
    return (
        <Route {...rest} render={() => {
            return localStorage.getItem("token") === "valid" ? children : <Redirect to="/" />
        }} />
    )
}

export default PrivateRoute;