


function TranslationItem({translation, images}) {
    
    const imageList = images.map((image, index) => <img src={"../" + image} alt="" key={index} style={{width: "100px", height: "100px"}} />)

    return (
        <li>
            <p>{translation}</p>
           <div>{imageList}</div>
        </li>
    )

}

export default TranslationItem