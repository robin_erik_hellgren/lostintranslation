import { useEffect, useState } from "react";
import { getConversionObject, getTranslation } from '../utils/utils';
import { getTranslationsByUserName, deleteTranslations } from "../utils/api";
import TranslationItem from "./TranslationItem";
import '../Profile.css';

function Profile() {
    const [activeUserState, setActiveUserState] = useState({ name: "", translation: [] });
    const [translationsState, setTranslationState] = useState([]);


    useEffect(() => {

        const activeUser = JSON.parse(localStorage.getItem("activeUser"));
        setActiveUserState(activeUser);

        getTranslationsByUserName(activeUser.name)
            .then(
                (translationsObjects) => {
                    const translations = translationsObjects.map(element => element.translation)
                    setTranslationState(translations);
                });

    }, []);

    function deleteHistory() {
        deleteTranslations(activeUserState.name);
        setTranslationState([]);
    }

    const conversionObject = getConversionObject();
    const translationList = translationsState.slice(translationsState.length - 10, translationsState.length).map((translation, index) => <TranslationItem key={index} translation={translation} images={getTranslation(translation, conversionObject)} />)

    return (
        <main>
            <h1>{activeUserState.name}'s Profile</h1>
            <button onClick={deleteHistory} className="deleteButton">Delete History</button>
            <ul>
                {translationList}
            </ul>
        </main>
    );
}

export default Profile;