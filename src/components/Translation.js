import { useState, useEffect } from "react";
import { addTranslation, getUserByName } from "../utils/api";
import { getConversionObject, getTranslation } from '../utils/utils'

function Translation({ setActiveUser }) {
    const [inputState, setInputState] = useState("");
    const [translationState, setTranslationState] = useState([]);
    const [conversionState, setconversionState] = useState({});


    useEffect(() => {
        const conversionObject = getConversionObject();
        setconversionState(conversionObject);

        setActiveUser(JSON.parse(localStorage.getItem("activeUser")).name);

    }, [setActiveUser]);

    function handleSubmit(e) {
        setTranslationState([]);
        const resultArray = getTranslation(inputState, conversionState);

        setTranslationState(resultArray);
        saveTranslation(inputState.toLowerCase());
        e.preventDefault();
    }

    function saveTranslation(translation) {
        getUserByName(JSON.parse(localStorage.getItem("activeUser")).name)
            .then(
                (activeUser) => {
                    console.log(activeUser);
                    addTranslation(translation, activeUser.id)
                })

    }

    const translationRender = translationState.map((element, index) => <img src={element} alt='' key={index} />)


    return (
        <div className="translationContainer">
            <div className="translationHeaderContainer">
                <form onSubmit={handleSubmit} className="translationForm">
                    <input type="text" onChange={(e) => setInputState(e.target.value)} value={inputState} placeholder="Type here..."/>
                    <button type="submit"><i className="fas fa-arrow-circle-right"></i></button>
                </form>
            </div>
            <main>
                <div className="translationBox">
                    {translationRender}
                </div>
                    <div className="translationBoxText">Translation</div>




            </main>
        </div>
    );
}

export default Translation;


