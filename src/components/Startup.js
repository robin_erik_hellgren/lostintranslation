import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";
import { postUser, getUserByName } from '../utils/api';

function Startup() {
    const [startupState, setStartupState] = useState({ name: "" });

    const history = useHistory();

    function handleSubmit(e) {
        getUserByName(startupState.name)
            .then((response) => {
                if (response !== undefined) {

                    const activeUser = {
                        name: response.name,
                        translation: response.translation,
                    }
                    console.log(activeUser)
                    localStorage.setItem('token', 'valid')
                    localStorage.setItem("activeUser", JSON.stringify(activeUser));
                    history.push("/translation")
                    console.log("DU FANNS")

                }
                else {
                    postUser(startupState.name);
                    alert("Your name was not in the database, now it's added and you can log in")
                }
            })
        e.preventDefault();
    }

    if (localStorage.getItem("token") === "valid") {
        return <Redirect to="/translation" />
    }

    return (
        <div className="startupMainContainer">
            <div className="startupHeaderContainer">
                <img src="/assets/Logo.png" alt="" />
                <div className="startupTextContainer">
                    <h1>Lost in Translation</h1>
                    <h2>Get Started</h2>
                </div>
            </div>
            <div className="startupFormContainer">
                <form className="startupForm"onSubmit={handleSubmit}>
                    <input type="text" onChange={(e) => setStartupState({ name: e.target.value })} value={startupState.name} placeholder="What's your name?"/>
                    <button type="submit"><i className="fas fa-arrow-circle-right"></i></button>
                </form>
            </div>
            <main>

            </main>
        </div>
    );
}

export default Startup;