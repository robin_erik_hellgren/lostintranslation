import { useEffect, useState } from "react";

function Logout() {
    const [tokenState, setTokenState] = useState("");

    useEffect(() => {
        if (localStorage.getItem("token") === "valid") {
            setTokenState("valid");
        }
    }, []);

    function logOut() {
        setTokenState("");
        localStorage.clear();
        window.location.href = '/';
    }

    return tokenState === "valid" || localStorage.getItem("token") === "valid" ?
        <button className="logoutButton" onClick={logOut}>Log Out</button> : 
        <p>Not logged in</p>;
}

export default Logout;