import './App.css';
import Profile from './components/Profile';
import Startup from './components/Startup';
import Translation from './components/Translation';
import PrivateRoute from './HOC/PrivateRoute';
import Logout from './components/Logout'
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import { useEffect, useState } from 'react';

function App() {

  const [activeUserState, setActiveUserState] = useState({});
  const [tokenState, setTokenState] = useState("");


  useEffect(() => {
    setTokenState(localStorage.getItem("token"));
  }, [tokenState]);

  const setActiveUser = (activeUser) => {
    setActiveUserState(activeUser);
  }

  const profileLink = Object.keys(activeUserState).length === 0 && activeUserState.constructor === Object ? "" : <div className="profileButtonContainer"><Link className="profileButton" to={"/profile/" + activeUserState}>{activeUserState}</Link><i className="fas fa-user-circle faPicture"></i></div>;

  return (
    <BrowserRouter className="App">
      <header>
        <div className="headerContainer">
          <Link to="/" className="headerLink">
            <h1>Lost in Translation</h1>
          </Link>
          <div className="profileButtonContainer">
            {profileLink}
            <Logout />
          </div>
        </div>
      </header>
      <div className="mainContainer">
        <Switch>
          <PrivateRoute path="/profile/:name">
            <Profile />
          </PrivateRoute>
          <PrivateRoute path="/translation">
            <Translation setActiveUser={setActiveUser} />
          </PrivateRoute>
          <Route path="/">
            <Startup />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
