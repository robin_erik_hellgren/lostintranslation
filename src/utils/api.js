const USERS_API_URL = "http://localhost:3001/users";
const TRANSLATIONS_API_URL = "http://localhost:3001/translations";


export const postUser = async (name) => {

  try {
    return fetch(USERS_API_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ name })
    });
  } catch (error) {
    console.log(error);
  }
}

export const getUserByName = async (name) => {

  try {
    const response = await fetch(USERS_API_URL + "?name=" + name);
    const response_1 = await response.json();
    return response_1[0];
  } catch (error) {
    console.log(error);
  }
}

export const getTranslationsByUserName = async (name) => {

  try {
    const user = await getUserByName(name);
    const response = await fetch(USERS_API_URL + "/" + user.id + "/translations?deleted=false" );
    const response_1 = await response.json();
    return response_1;
  } catch (error) {
    console.log(error);
  }
}


export const addTranslation = (translation, id) => {

  fetch(TRANSLATIONS_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ 
      deleted: false,
      translation: translation,
      userId: id,
    })
  })
  .catch((error) => {
    console.log(error);
  });
}

export const deleteTranslations = async (name) => {
  const user = await getUserByName(name);
  const translationsResponse = await fetch(TRANSLATIONS_API_URL + "?userId=" + user.id + "&deleted=false");
  const translations = await translationsResponse.json();
  translations.forEach(element => element.deleted = true)
  console.log(translations);

  Promise.all(translations.map(element => {
    return fetch(TRANSLATIONS_API_URL + "/" + element.id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(element)
    })
  })).catch((error)=> {console.log(error)})

}