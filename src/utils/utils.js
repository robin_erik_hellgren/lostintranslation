export const  getConversionObject = () => {
    const conversionObject = {};
    for (let i = 0; i < 26; i++) {

        conversionObject[String.fromCharCode(97 + i)] = "assets/individial_signs/" + String.fromCharCode(97 + i) + ".png";
    }
    return conversionObject;
}

export const getTranslation = (inputState, conversionState) => {
    const inputArray = inputState.toLowerCase().split("");
    const resultArray = [];
    inputArray.forEach(element => {
        resultArray.push(conversionState[element]);
    });
    return resultArray;
}

